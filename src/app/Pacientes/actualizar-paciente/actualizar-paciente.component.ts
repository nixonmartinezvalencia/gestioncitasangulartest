import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Paciente } from 'src/app/Modelo/Paciente';
import { PacienteServiceService } from 'src/app/Services/paciente-service.service';

@Component({
  selector: 'app-actualizar-paciente',
  templateUrl: './actualizar-paciente.component.html',
 
})
export class ActualizarPacienteComponent implements OnInit {

  paciente :Paciente = new Paciente();
  constructor(private service:PacienteServiceService, private router:Router) { }

  ngOnInit() {
    
  }


}
